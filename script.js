const numberInput = () => {
  const input = document.querySelector("#inputBox").value;

  getAnswer(input);
};

const getAnswer = (expression = "") => {
  const answerField = document.querySelector("#answerParagraph");
  expression = expression.replaceAll(" ", "");

  const inDegree = document.querySelector("#degree-checkbox").checked;

  while (expression.indexOf("resin(") !== -1) {
    expression = handleResin(expression);
  }

  if (inDegree) {
    expression = handleDegree(expression, "sin");
    expression = handleDegree(expression, "cos");
  }

  try {
    const answer = Number(math.evaluate(expression));
    if (String(answer) == "NaN") throw new Error();

    answerField.innerHTML = answer;
    answerField.classList = "answerParagraph";
  } catch (error) {
    answerField.classList = "answerParagraph answer_error";
    answerField.innerHTML = "Syntax Error";
  }
};

const handleDegree = (expression = "", value) => {
  let beginIndex = expression.indexOf(`${value}(`);

  if (beginIndex === -1) return expression;

  beginIndex += 4;

  let temp = expression.substring(beginIndex);

  let endIndex = temp.indexOf(")") + beginIndex;

  temp = temp.substring(0, endIndex - beginIndex);

  for (let i = 0; i < temp.length; i++) {
    if (temp[i] === "(") {
      temp = expression.substring(endIndex + 1);

      endIndex = temp.indexOf(")") + endIndex + 1;
    }
  }
  expression = expression.replace(
    expression.substring(beginIndex - 4, endIndex + 1),
    value + " (" + expression.substring(beginIndex, endIndex) + "*(180/pi))"
  );

  return handleDegree(expression, value);
};

const handleResin = (expression = "") => {
  let beginIndex = expression.indexOf("resin(");

  if (beginIndex === -1) return expression;

  beginIndex += 6;

  let temp = expression.substring(beginIndex);

  let endIndex = temp.indexOf(")") + beginIndex;

  temp = temp.substring(0, endIndex - beginIndex);

  for (let i = 0; i < temp.length; i++) {
    if (temp[i] === "(") {
      temp = expression.substring(endIndex + 1);

      endIndex = temp.indexOf(")") + endIndex + 1;
    }
  }
  expression = expression.replace(
    expression.substring(beginIndex - 6, endIndex + 1),
    "(" + expression.substring(beginIndex, endIndex) + ")*392.9"
  );
  return expression;
};
