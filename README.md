# ClassCalc Challenge 1

This is a super simple calculator.

## Comments

Hi team MightyByte, I have finished the challenge but I realized there are some issues with your test cases.

If you do the same calculations by hand, calculator or a console in a browswer, the answer is different from the values in your test cases.

You seem to have some issues when you did calculations for your test cases espcially those involving `pi` and `degree`

### Here are some test cases examples which are wrong.

| expression                        | Your Answer  | Correct Answer      |
| --------------------------------- | ------------ | ------------------- |
| In Degree 5(sin(3)-cos(pi\*15))/4 | -0.785099... | 1.2263300873768908  |
| In Degree sin(5.7)                | 0.099319...  | -0.1392388629788243 |
| In Degree cos(pi)                 | 0.99849...   | -0.5984600690578581 |

## Examples you can try.

| expression                        | Parsed Expression                                                  |
| --------------------------------- | ------------------------------------------------------------------ |
| In Degree 5(sin(3)-cos(pi\*15))/4 | 5*(Math.sin(3*(180/Math.PI))-Math.cos(Math.PI*15*(180/Math.PI)))/4 |

But other than that, I believe my solution seems to do the trick of what you are looking for.
